package com.br.sic.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.br.sic.model.Compra;
import com.br.sic.vo.ItemCompraVO;

@Repository
public interface CompraRepository extends JpaRepository<Compra, Long> {

	public List<ItemCompraVO> findItensPendentes();
	
	public void deleteByVenda(Long idVenda);

}
