package com.br.sic.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.br.sic.model.Produto;

@Repository
public interface ProdutoRepository extends JpaRepository<Produto, Long> {
	
	@Query(value = "select p.* from produto p where p.codigo = ?1", nativeQuery=true)
	public Produto findByCodigo(Long codigo);
	
	@Query(value = "select max(p.codigo) from produto p", nativeQuery=true)
	public Long ultimoCodigoSalvo();

}
