package com.br.sic.repositories;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class CompraDaoImpl implements CompraRepository {
	
	@Autowired
    SessionFactory sessionFactory;
    
	@Override
	public void deleteByVenda(Long idVenda) {
		sessionFactory.getCurrentSession().delete(idVenda);
	}


}
