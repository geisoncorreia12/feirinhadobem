package com.br.sic.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.br.sic.model.ItemVenda;

@Repository
public interface ItemVendaRepository extends JpaRepository<ItemVenda, Long> {
	
	@Query(value = "select i.* from item_venda i inner join venda v on (i.id_venda = v.id) where v.id = ?1", nativeQuery=true)
	public List<ItemVenda> findPorVenda(Long idVenda);

}
