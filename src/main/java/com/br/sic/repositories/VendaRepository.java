package com.br.sic.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.br.sic.model.Venda;

@Repository
public interface VendaRepository extends JpaRepository<Venda, Long> {
	
	@Query(value = "select v.* from venda v where v.data_fim is null and v.data_cancelamento is null", nativeQuery=true)
	List<Venda> buscarTodosPedidosAtivos();

	@Query(value = "select v.* from venda v where v.data_fim is not null or v.data_cancelamento is not null", nativeQuery=true)
	List<Venda> buscarTodosPedidosFinalizados();

}
