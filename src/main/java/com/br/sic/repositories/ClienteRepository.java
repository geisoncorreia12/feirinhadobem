package com.br.sic.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.br.sic.model.Cliente;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Long> {
	
//	@Query("SELECT c FROM Cliente c WHERE UPPER(c.nome) LIKE '%', :nome, '%'))")
//	@Query("Select c from Cliente c where c.nome like %:nome%")
//	@Query(value = "select c.* from cliente c where c.nome like %:nome%", nativeQuery=true)
	public List<Cliente> findByNome(String nome);

	public List<Cliente> findByNomeLike(String nome);
	
}
