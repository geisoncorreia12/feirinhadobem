package com.br.sic.servicerest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

public class NetClientGet {

	// http://localhost:8080/RESTfulExample/json/product/get
	public static void main(String[] args) {

	  try {

		URL url = new URL("https://magazen.casamagalhaes.services/v1/users/find-by?cnpj=03086268000186&ativo=true");
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("GET");
		conn.setRequestProperty("Accept", "application/json");

		if (conn.getResponseCode() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "
					+ conn.getResponseCode());
		}

		BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
		
		System.out.println("Output from Server .... \n");
		
		TypeReference<List<Retorno>> typeReference = new TypeReference<List<Retorno>>() {};

		List<Retorno> listaRetorno = new ObjectMapper().readValue(br.readLine(), typeReference);
		
		listaRetorno.forEach(i -> System.out.println(i.getId()));
		
		//System.out.println(retorno.getId());
		
		conn.disconnect();

	  } catch (MalformedURLException e) {

		e.printStackTrace();

	  } catch (IOException e) {

		e.printStackTrace();

	  }

	}
	
	@JsonIgnoreProperties( ignoreUnknown = true )
	public static class Retorno {
		
		@XmlElement(required = true)
		@JsonProperty("id")
		private String id;
		
		@XmlElement(required = true)
		@JsonProperty("cnpj")
		private String cnpj;
		
		@XmlElement(required = true)
		@JsonProperty("nome")
		private String nome;
		
		@XmlElement(required = true)
		@JsonProperty("email")
		private String email;
		
		@XmlElement(required = true)
		@JsonProperty("ativo")
		private String ativo;
		
		@XmlElement(required = true)
		@JsonProperty("zendeskId")
		private String zendeskId;
		
		@XmlElement(required = true)
		@JsonProperty("zendeskOrganizationId")
		private String zendeskOrganizationId;
		
		@XmlElement(required = true)
		@JsonProperty("zendeskOrganizations")
		private List<ZendeskOrganizationsType> zendeskOrganizations;
		 
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getCnpj() {
			return cnpj;
		}
		public void setCnpj(String cnpj) {
			this.cnpj = cnpj;
		}
		public String getNome() {
			return nome;
		}
		public void setNome(String nome) {
			this.nome = nome;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		public String getAtivo() {
			return ativo;
		}
		public void setAtivo(String ativo) {
			this.ativo = ativo;
		}
		public String getZendeskId() {
			return zendeskId;
		}
		public void setZendeskId(String zendeskId) {
			this.zendeskId = zendeskId;
		}
		public String getZendeskOrganizationId() {
			return zendeskOrganizationId;
		}
		public void setZendeskOrganizationId(String zendeskOrganizationId) {
			this.zendeskOrganizationId = zendeskOrganizationId;
		}
		public List<ZendeskOrganizationsType> getZendeskOrganizations() {
			return zendeskOrganizations;
		}
		public void setZendeskOrganizations(List<ZendeskOrganizationsType> zendeskOrganizations) {
			this.zendeskOrganizations = zendeskOrganizations;
		}
		
	}
	
	@JsonIgnoreProperties( ignoreUnknown = true )
	public static class ZendeskOrganizationsType {
		
		@XmlElement(required = true)
		@JsonProperty("id")
		private String id;
		
		@XmlElement(required = true)
		@JsonProperty("cnpj")
		private String cnpj;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getCnpj() {
			return cnpj;
		}

		public void setCnpj(String cnpj) {
			this.cnpj = cnpj;
		}
		
	}

}
