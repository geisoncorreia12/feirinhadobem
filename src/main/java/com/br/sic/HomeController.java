package com.br.sic;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.br.sic.model.Cliente;
import com.br.sic.model.Venda;
import com.br.sic.services.VendaService;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;

@Controller
public class HomeController {

	@Autowired
	private VendaService vendaService;
	
	private ModelAndView mv;

	@GetMapping("/")
	public ModelAndView home() {

		List<Venda> vendas = vendaService
				.buscarTodosPedidosAtivos()
					.stream().
						sorted((v1, v2) -> v2.getDataVenda().compareTo(v1.getDataVenda())).
							collect(Collectors.toList());

		List<Venda> pedidosFinalizados = vendaService
				.buscarTodosPedidosFinalizados()
					.stream().
						sorted(Comparator.comparing(Venda::getDataVenda)).
							collect(Collectors.toList());
		
//		List<Venda> listaASC = pedidosFinalizados.stream().sorted(Comparator.comparing(Venda::getDataVenda)).collect(Collectors.toList());
//		List<Venda> listaDESC = pedidosFinalizados.stream().sorted(Comparator.comparing(Venda::getDataVenda).reversed()).collect(Collectors.toList());
		
		/*System.out.println("listaASC **********");
		listaASC.forEach((v) -> System.out.println(v.getCliente().getNome()));
		System.out.println("listaDESC **********");
		listaDESC.forEach((v) -> System.out.println(v.getCliente().getNome()));*/
		
		mv = new ModelAndView("/index");
		mv.addObject("pedidos", vendas);
		mv.addObject("pedidosFinalizados", pedidosFinalizados);
		return mv;
	}
	

	@GetMapping("/home/view/{id}")
	public ModelAndView view(@PathVariable("id") Long id) {
		return addView(vendaService.findById(id));
	}

	@GetMapping("/home/add/view")
	public ModelAndView addView(Venda pedido) {

		ModelAndView mv = new ModelAndView("/home/view");
		mv.addObject("pedido", pedido);
		mv.addObject("itens", pedido.getItens());

		pedido.calculaSomaTotal();

		return mv;
	}

	@RequestMapping(value = "/home/relatorioVenda/{id}", method = RequestMethod.GET)
	@ResponseBody
	public void gerarRelatorio(HttpServletResponse response, @PathVariable("id") Long id)
			throws JRException, IOException {

		Venda venda = vendaService.findById(id);
		Cliente cliente = venda.getCliente();

		venda.calculaSomaTotal();

		InputStream jasperStream = this.getClass().getResourceAsStream("/reports/rel_resumo_venda_cliente.jasper");
		Map<String, Object> params = new HashMap<>();
		params.put("NOME_CLIENTE", cliente.getNome());
		params.put("FONE_CLIENTE", cliente.getTelefone());
		params.put("ENDERECO_ENTREGA", cliente.getEnderecoEntrega().enderecoMontado());
		params.put("DATE_PEDIDO", venda.getDataVenda());
		params.put("DATE_ENTREGA", venda.getDataEntrega());
		params.put("VALOR_TOTAL", venda.getValorTotal().add(venda.getValorFrete()));
		params.put("VALOR_FRETE", venda.getValorFrete());
		JasperReport jasperReport = (JasperReport) JRLoader.loadObject(jasperStream);
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params,
				new JRBeanCollectionDataSource(venda.getItens()));
		// JasperViewer.viewReport(jasperPrint, false);
		response.setContentType("application/x-pdf");
		response.setHeader("Content-disposition", "inline; filename=rel_venda_"+cliente.getNome()+"_"+venda.getDataVenda());

		final OutputStream outStream = response.getOutputStream();
		JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);
	}

	@RequestMapping(value = "/home/finalizarVenda/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ModelAndView finanizarPedido(HttpServletResponse response, @PathVariable("id") Long id) {
		
		Venda venda = vendaService.findById(id);
		venda.setDataFim(new Date());
		vendaService.edit(venda);
		
		List<Venda> pedidos = new ArrayList<>();
		pedidos = vendaService.buscarTodosPedidosAtivos();
		
		mv.addObject("pedidos", pedidos );
		mv.addObject("pedidosFinalizados", vendaService.buscarTodosPedidosFinalizados());
		return mv;
		

	}
	
	@RequestMapping(value = "/home/cancelarVenda/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ModelAndView cancelarPedido(HttpServletResponse response, @PathVariable("id") Long id) {
		
		Venda venda = vendaService.findById(id);
		venda.setDataCancelamento(new Date());
		vendaService.edit(venda);
		
		List<Venda> pedidos = new ArrayList<>();
		pedidos = vendaService.buscarTodosPedidosAtivos();
		
		mv.addObject("pedidos", pedidos );
		mv.addObject("pedidosFinalizados", vendaService.buscarTodosPedidosFinalizados());
		return mv;
		

	}
}
