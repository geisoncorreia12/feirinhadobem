package com.br.sic.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Table(name="PRODUTO", schema="sic")
@Entity(name="PRODUTO")
public class Produto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7623259127601572140L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="ID_TIPO_PRODUTO")
	private TipoProduto tipoProduto;

	@ManyToOne
	@JoinColumn(name="ID_CATEGORIA_PRODUTO")
	private CategoriaProduto categoriaProduto;
	
	@ManyToOne
	@JoinColumn(name="ID_FORNECEDOR")
	private Fornecedor fornecedor;

	@ManyToOne
	@JoinColumn(name="ID_UNIDADE_MEDIDA")
	private UnidadeMedida unidadeMedida;
	
	@Column(name="DESCRICAO")
	private String descricao;
	
	@Column(name="CODIGO")
	private Long codigo;
	
	@Column(name="PRECO_VENDA")
	private BigDecimal precoVenda;

	@Column(name="PRECO_COMPRA")
	private BigDecimal precoCompra;
	
	@Column(name="DATA_COMPRA")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCompra;
	
	@Transient
	private String precoCompraStr;
	@Transient
	private String precoVendaStr;

	public String tipo() {
		return getTipoProduto() != null ? getTipoProduto().getDescricao() : "";
	}
	
	public String categoria() {
		return getCategoriaProduto() != null ? getCategoriaProduto().getDescricao() : "";
	}

	public String unidade() {
		return getUnidadeMedida() != null ? getUnidadeMedida().getDescricao() : "";
	}

	public String fornecedor() {
		return getFornecedor() != null ? getFornecedor().getNome() : "";
	}
	
	public TipoProduto getTipoProduto() {
		return tipoProduto;
	}
	
	public void setTipoProduto(TipoProduto tipoProduto) {
		this.tipoProduto = tipoProduto;
	}
	
	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	public CategoriaProduto getCategoriaProduto() {
		return categoriaProduto;
	}

	public void setCategoriaProduto(CategoriaProduto categoriaProduto) {
		this.categoriaProduto = categoriaProduto;
	}

	public Produto() {	}

	public Produto(Long codigo, String descricao, BigDecimal precoCompra, BigDecimal precoVenda, Date dataCompra) {
		super();
		this.codigo = codigo;
		this.descricao = descricao;
		this.precoVenda = precoVenda;
		this.dataCompra = dataCompra;
	}

	public Produto(Long codigo) {
		this.codigo = codigo;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Date getDataCompra() {
		return dataCompra;
	}

	public void setDataCompra(Date dataCompra) {
		this.dataCompra = dataCompra;
	}
	
	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public BigDecimal getPrecoVenda() {
		return precoVenda;
	}

	public void setPrecoVenda(BigDecimal precoVenda) {
		this.precoVenda = precoVenda;
	}
	
	public BigDecimal getPrecoCompra() {
		return precoCompra;
	}

	public void setPrecoCompra(BigDecimal precoCompra) {
		this.precoCompra = precoCompra;
	}
	
	public UnidadeMedida getUnidadeMedida() {
		return unidadeMedida;
	}

	public void setUnidadeMedida(UnidadeMedida unidadeMedida) {
		this.unidadeMedida = unidadeMedida;
	}
	
	public String getPrecoCompraStr() {
		return precoCompraStr;
	}

	public void setPrecoCompraStr(String precoCompraStr) {
		this.precoCompraStr = precoCompraStr;
	}

	public String getPrecoVendaStr() {
		return precoVendaStr;
	}

	public void setPrecoVendaStr(String precoVendaStr) {
		this.precoVendaStr = precoVendaStr;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Produto other = (Produto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
