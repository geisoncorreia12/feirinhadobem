package com.br.sic.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Table(name="ITEM_COMPRA", schema="sic")
@Entity(name="ITEM_COMPRA")
public class ItemCompra implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4363094145076132553L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID")
	private Long id;
	
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="ID_COMPRA")
	private Compra compra;
	
	@ManyToOne
	@JoinColumn(name="ID_TIPO_PRODUTO")
	private TipoProduto tipoProduto;

	@ManyToOne
	@JoinColumn(name="ID_CATEGORIA_PRODUTO")
	private CategoriaProduto categoriaProduto;
	
	@ManyToOne
	@JoinColumn(name="ID_FORNECEDOR")
	private Fornecedor fornecedor;

	@ManyToOne
	@JoinColumn(name="ID_UNIDADE_MEDIDA")
	private UnidadeMedida unidadeMedida;
	
	@Column(name="DESCRICAO")
	private String descricao;
	
	@Column(name="CODIGO")
	private Long codigo;
	
	@Column(name="QUANTIDADE")
	private BigDecimal quantidade;
	
	@Column(name="PRECO_COMPRA")
	private BigDecimal precoCompra;
	
	public BigDecimal calculaValorCompra() {
		return getPrecoCompra().multiply(getQuantidade()).setScale(2, BigDecimal.ROUND_HALF_UP); 
	}
	
	public String tipo() {
		return getTipoProduto() != null ? getTipoProduto().getDescricao() : "";
	}
	
	public String categoria() {
		return getCategoriaProduto() != null ? getCategoriaProduto().getDescricao() : "";
	}

	public String unidade() {
		return getUnidadeMedida() != null ? getUnidadeMedida().getDescricao() : "";
	}

	public String fornecedor() {
		return getFornecedor() != null ? getFornecedor().getNome() : "";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public UnidadeMedida getUnidadeMedida() {
		return unidadeMedida;
	}

	public void setUnidadeMedida(UnidadeMedida unidadeMedida) {
		this.unidadeMedida = unidadeMedida;
	}

	public BigDecimal getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(BigDecimal quantidade) {
		this.quantidade = quantidade;
	}

	public Compra getCompra() {
		return compra;
	}

	public void setCompra(Compra compra) {
		this.compra = compra;
	}
	
	public BigDecimal getPrecoCompra() {
		return precoCompra;
	}

	public void setPrecoCompra(BigDecimal precoCompra) {
		this.precoCompra = precoCompra;
	}
	
	public TipoProduto getTipoProduto() {
		return tipoProduto;
	}

	public void setTipoProduto(TipoProduto tipoProduto) {
		this.tipoProduto = tipoProduto;
	}

	public CategoriaProduto getCategoriaProduto() {
		return categoriaProduto;
	}

	public void setCategoriaProduto(CategoriaProduto categoriaProduto) {
		this.categoriaProduto = categoriaProduto;
	}

	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItemCompra other = (ItemCompra) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
