package com.br.sic.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@NamedQuery(name = "Cliente.findByName", query = "SELECT c FROM Cliente c WHERE LOWER(c.nome) = LOWER(?1)")
@Table(name="CLIENTE", schema="sic")
public class Cliente implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8773114726684920711L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID")
	private Long id;
	
	@Column(name="NOME")
	private String nome;

	@Column(name="SOBRE_NOME")
	private String sobreNome;
	
	@ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="ID_ENDERECO")
	private Endereco endereco;

	@ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="ID_ENDERECO_ENTREGA")
	private Endereco enderecoEntrega;
	
	@Column(name="EMAIL")
	private String email;
	
	@Column(name="TELEFONE")
	private String telefone;
	
	public String enderecoCompleto() {
		
		StringBuffer endereco = new StringBuffer();
		String virgula = ", ";
		
		if(getEndereco() != null) {
			String logradouro = getEndereco().getLogradouro();
			String complemento = getEndereco().getComplemento();
			String numero = getEndereco().getNumero();
			String bairro = getEndereco().getBairro();
			
			if(null != logradouro && !logradouro.isEmpty()) {
				endereco.append(logradouro).append(virgula);
			}
			if(null != complemento && !complemento.isEmpty()) {
				endereco.append(complemento).append(virgula);
			}
			if(null != numero && !numero.isEmpty()) {
				endereco.append(numero).append(virgula);
			}
			if(null != bairro && !bairro.isEmpty()) {
				endereco.append(bairro);
			}
			
		}
		
		return endereco.toString();
	}
	
	public String enderecoEntregaCompleto() {
		
		StringBuffer endereco = new StringBuffer();
		String virgula = ", ";
		
		if(getEndereco() != null) {
			String logradouro = getEnderecoEntrega().getLogradouro();
			String complemento = getEnderecoEntrega().getComplemento();
			String numero = getEnderecoEntrega().getNumero();
			String bairro = getEnderecoEntrega().getBairro();
			
			if(null != logradouro && !logradouro.isEmpty()) {
				endereco.append(logradouro).append(virgula);
			}
			if(null != complemento && !complemento.isEmpty()) {
				endereco.append(complemento).append(virgula);
			}
			if(null != numero && !numero.isEmpty()) {
				endereco.append(numero).append(virgula);
			}
			if(null != bairro && !bairro.isEmpty()) {
				endereco.append(bairro);
			}
			
		}
		
		return endereco.toString();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	
	public Endereco getEnderecoEntrega() {
		return enderecoEntrega;
	}

	public void setEnderecoEntrega(Endereco enderecoEntrega) {
		this.enderecoEntrega = enderecoEntrega;
	}
	
	public String getSobreNome() {
		return sobreNome;
	}

	public void setSobreNome(String sobreNome) {
		this.sobreNome = sobreNome;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cliente other = (Cliente) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
