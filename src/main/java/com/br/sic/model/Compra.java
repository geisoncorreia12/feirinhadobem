package com.br.sic.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQuery;
import javax.persistence.OneToMany;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.br.sic.vo.ItemCompraVO;

@Table(name="COMPRA", schema="sic")
@Entity(name="COMPRA")
@SqlResultSetMapping(
        name = "itensDacompra",
        classes = {
                @ConstructorResult(
                        targetClass = ItemCompraVO.class,
                        columns = {
                        		@ColumnResult(name = "dataCompra",type=Date.class),
                                @ColumnResult(name = "codigo",type=Long.class),
                                @ColumnResult(name = "descricao", type=String.class),
                                @ColumnResult(name = "unidadeMedida", type=String.class),
                                @ColumnResult(name = "quantidade", type=BigDecimal.class),
                                @ColumnResult(name = "precoCompra", type=BigDecimal.class)
                        }
                )
        }
)
@NamedNativeQuery(
        name = "Compra.findItensPendentes",
        query = "select c.data_compra as dataCompra, i.codigo as codigo, i.descricao as descricao, u.descricao as unidadeMedida, i.preco_compra as precoCompra, sum(i.quantidade) as quantidade from sic.item_compra i inner join sic.compra c on (i.id_compra = c.id) inner join sic.unidade_medida u on (i.id_unidade_medida = u.id) inner join sic.venda v on (c.id_venda = v.id) where v.data_fim is null group by i.descricao",
        resultSetMapping = "itensDacompra"
)
public class Compra implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3066890222626241845L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID")
	private Long id;
	
	@Column(name="DATA_COMPRA")
	private Date dataCompra;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_VENDA")
	private Venda venda;

	@Transient
	private String dataCompraStr;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "compra", cascade=CascadeType.ALL)
	private List<ItemCompra> itens;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDataCompra() {
		return dataCompra;
	}

	public void setDataCompra(Date dataCompra) {
		this.dataCompra = dataCompra;
	}

	public String getDataCompraStr() {
		return dataCompraStr;
	}

	public void setDataCompraStr(String dataCompraStr) {
		this.dataCompraStr = dataCompraStr;
	}
	
	public List<ItemCompra> getItens() {
		return itens;
	}

	public void setItens(List<ItemCompra> itens) {
		this.itens = itens;
	}
	
	public Venda getVenda() {
		return venda;
	}

	public void setVenda(Venda venda) {
		this.venda = venda;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Compra other = (Compra) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}