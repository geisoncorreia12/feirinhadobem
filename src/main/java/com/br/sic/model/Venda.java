package com.br.sic.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Table(name="VENDA", schema="sic")
@Entity(name="VENDA")
public class Venda implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7623259127601572140L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID")
	private Long id;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_CLIENTE")
	private Cliente cliente;

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_TIPO_PAGAMENTO")
	private TipoPagamento tipoPagamento;
	
	@Column(name="VALOR_DESCONTO")
	private BigDecimal valorDesconto;

	@Column(name="VALOR_FRETE")
	private BigDecimal valorFrete;
	
	@Column(name="OBSERVACAO")
	private String observacao;
	
	@Column(name="DATA_ENTREGA")
	@Temporal(TemporalType.DATE)
	private Date dataEntrega;

	@Column(name="DATA_VENDA")
	@Temporal(TemporalType.DATE)
	private Date dataVenda;

	@Column(name="DATA_CANCELAMENTO")
	@Temporal(TemporalType.DATE)
	private Date dataCancelamento;
	
	@Transient
	private ItemVenda item;
	@Transient
	private String tipoPagamentoStr;
	@Transient
	private String valorDescontoStr;
	@Transient
	private String valorFreteStr;
	@Transient
	private String dataEntregaStr;
	@Transient
	private BigDecimal valorTotal;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "venda", cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
	private List<ItemVenda> itens;

	@Column(name="DATA_FIM")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataFim;
	
	public Venda() {	}
	
	public BigDecimal calculaSomaTotal() {
		
		BigDecimal valorTotal =  new BigDecimal(BigInteger.ZERO);
		
		if(getItens() != null && !getItens().isEmpty()) {
			for (ItemVenda itemVenda : getItens()) {
				valorTotal = valorTotal.add(itemVenda.calculaValorVenda());
			}
		}
		setValorTotal(valorTotal);
		
		return getValorTotal();
	}
	
	public String statusVenda() {
		if(getDataCancelamento() != null) {
			return "Venda cancelada";
		} else {
			return "Venda faturada";
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public BigDecimal getValorDesconto() {
		return valorDesconto;
	}

	public void setValorDesconto(BigDecimal valorDesconto) {
		this.valorDesconto = valorDesconto;
	}

	public BigDecimal getValorFrete() {
		return valorFrete;
	}

	public void setValorFrete(BigDecimal valorFrete) {
		this.valorFrete = valorFrete;
	}

	public Date getDataEntrega() {
		return dataEntrega;
	}

	public void setDataEntrega(Date dataEntrega) {
		this.dataEntrega = dataEntrega;
	}

	public Date getDataVenda() {
		return dataVenda;
	}

	public void setDataVenda(Date dataVenda) {
		this.dataVenda = dataVenda;
	}
	
	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public BigDecimal getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(BigDecimal valorTotal) {
		this.valorTotal = valorTotal;
	}
	
	public List<ItemVenda> getItens() {
		return itens;
	}

	public void setItens(List<ItemVenda> itens) {
		this.itens = itens;
	}
	
	public TipoPagamento getTipoPagamento() {
		return tipoPagamento;
	}

	public void setTipoPagamento(TipoPagamento tipoPagamento) {
		this.tipoPagamento = tipoPagamento;
	}
	
	public Date getDataFim() {
		return dataFim;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}
	
	public ItemVenda getItem() {
		return item;
	}

	public void setItem(ItemVenda item) {
		this.item = item;
	}
	
	public String getTipoPagamentoStr() {
		return tipoPagamentoStr;
	}

	public void setTipoPagamentoStr(String tipoPagamentoStr) {
		this.tipoPagamentoStr = tipoPagamentoStr;
	}

	public String getValorDescontoStr() {
		return valorDescontoStr;
	}

	public void setValorDescontoStr(String valorDescontoStr) {
		this.valorDescontoStr = valorDescontoStr;
	}

	public String getValorFreteStr() {
		return valorFreteStr;
	}

	public void setValorFreteStr(String valorFreteStr) {
		this.valorFreteStr = valorFreteStr;
	}

	public String getDataEntregaStr() {
		return dataEntregaStr;
	}

	public void setDataEntregaStr(String dataEntregaStr) {
		this.dataEntregaStr = dataEntregaStr;
	}
	
	public Date getDataCancelamento() {
		return dataCancelamento;
	}

	public void setDataCancelamento(Date dataCancelamento) {
		this.dataCancelamento = dataCancelamento;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Venda other = (Venda) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}