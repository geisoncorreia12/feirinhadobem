package com.br.sic;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.br.sic.model.Cliente;
import com.br.sic.model.Compra;
import com.br.sic.model.ItemCompra;
import com.br.sic.model.ItemVenda;
import com.br.sic.model.Produto;
import com.br.sic.model.UnidadeMedida;
import com.br.sic.model.Venda;
import com.br.sic.services.ClienteService;
import com.br.sic.services.CompraService;
import com.br.sic.services.ItemVendaService;
import com.br.sic.services.ProdutoService;
import com.br.sic.services.TipoPagamentoService;
import com.br.sic.services.UnidadeMedidaService;
import com.br.sic.services.VendaService;
import com.br.sic.utils.DataUtils;
import com.br.sic.vo.VendaVO;

@Controller(value="vendaController")
public class VendaController {
	
	@Autowired
	private VendaService vendaService;

	@Autowired
	ItemVendaService itemVendaService;
	
	@Autowired
	CompraService compraService;

	@Autowired
	private ProdutoService produtoService;

	@Autowired
	private UnidadeMedidaService unidadeMedidaService;

	@Autowired
	private TipoPagamentoService tipoPagamentoService;

	@Autowired
	private ClienteService clienteService;
	
	private ModelAndView mv; 
	
	private Long codigoProduto;
	
	private Cliente cliente;

	private Produto produto;

	private VendaVO venda;
	private Venda pedido;

	private List<ItemVenda> itensVenda;
	private ItemVenda itemVenda;
	
	@GetMapping("/vendas")
    public ModelAndView venda() {
        
		setItensVenda(new ArrayList<ItemVenda>());
		setVenda(new VendaVO());
		setProduto(new Produto());
		
		mv = new ModelAndView("/vendas/registrar");
        mv.addObject("venda", getVenda());
        mv.addObject("enderecoCompleto", "");
        mv.addObject("unidades", unidadeMedidaService.findAll());
        mv.addObject("tipoPagamentos", tipoPagamentoService.findAll());
        mv.addObject("itens", getItensVenda());
        

		/*.stream()
		.sorted((item1, item2) -> Integer.compare(item1.getDescricao().length(), item2.getDescricao().length()))*/
        
        return mv;
    }
	
	@PostMapping("/vendas/edit")
	public ModelAndView edit(@Valid Venda pedido, BindingResult result) {
		
		pedido.setDataEntrega(new DateTime(pedido.getDataEntregaStr()).toDate());
		pedido.setValorFrete(!pedido.getValorFreteStr().isEmpty() ?
				new BigDecimal(pedido.getValorFreteStr().replace("R$", "").replace(",", ".").trim())
				: new BigDecimal("0"));
		pedido.setValorDesconto(
				!pedido.getValorDescontoStr().isEmpty() ? 
						new BigDecimal(pedido.getValorDescontoStr().replace("R$", "").replace(",", ".").trim()) 
						: new BigDecimal("0"));
	
		pedido.setItens(getPedido().getItens());

		vendaService.create(pedido);
		
		List<ItemVenda> itensSalvos = itemVendaService.findPorVenda(getPedido().getId());
		List<ItemVenda> itensRemovidos = new ArrayList<ItemVenda>();
		
		for(ItemVenda item: pedido.getItens()) {
			item.setVenda(pedido);
			itemVendaService.edit(item);
		}

		//remover itens removidos. Isso pq o cascade não funciona. 
		for(ItemVenda itSalvo: itensSalvos) {
			if(!pedido.getItens().contains(itSalvo)) {
				itensRemovidos.add(itSalvo);
			}
		}

		if(!itensRemovidos.isEmpty()) {
			for(ItemVenda i: itensRemovidos) {
				itemVendaService.deleteById(i.getId());
			}
		}
		
//		compraService.deleteByVenda(pedido.getId());
		/*Compra compra = criateCompra(pedido);
		compra.setVenda(pedido);
		compraService.create(compra);*/
		
		return addEdit(pedido);
	}
	
	@GetMapping("/vendas/edit/{id}")
	public ModelAndView edit(@PathVariable("id") Long id) {
		setPedido(vendaService.findById(id));
		
		getPedido().setDataEntregaStr(DataUtils.dataFormatada("dd/MM/yyyy", getPedido().getDataEntrega()));
		getPedido().setValorFreteStr(getPedido().getValorFrete()!= null ? getPedido().getValorFrete().toString() : "");
		getPedido().setValorDescontoStr(getPedido().getValorDesconto()!= null ? getPedido().getValorDesconto().toString() : "");
		
		return addEdit(getPedido());
	}

	@GetMapping("/vendas/add/edit")
	public ModelAndView addEdit(Venda pedido) {
		
		ModelAndView mv = new ModelAndView("/home/edit");
		mv.addObject("pedido", pedido);
		mv.addObject("itens", pedido.getItens());
		mv.addObject("tipoPagamentos", tipoPagamentoService.findAll());
		
		pedido.calculaSomaTotal();
		
		return mv;
	}
	
    @PostMapping("/vendas/save")
    public ModelAndView save(@Valid VendaVO venda, BindingResult result) {
    
    	Venda vendaPersiste = new Venda();
    	
    	List<Cliente> clientes = clienteService.findByNome(venda.getCliente().getNome());
    	
    	if(!clientes.isEmpty()) {
    		Cliente cliente = !clientes.isEmpty() ? clientes.get(0) : null;
    		
    		vendaPersiste.setCliente(cliente);
    		vendaPersiste.setDataVenda(new Date());
    		vendaPersiste.setDataEntrega(venda.getVenda().getDataEntrega());
    		vendaPersiste.setObservacao(venda.getVenda().getObservacao());
    		
    		vendaPersiste.setValorFrete(!venda.getVenda().getValorFreteStr().isEmpty() ?
    				new BigDecimal(venda.getVenda().getValorFreteStr().replace("R$", "").replace(",", ".").trim())
    				: new BigDecimal("0"));
    		vendaPersiste.setValorDesconto(
    				!venda.getVenda().getValorDescontoStr().isEmpty() ? 
    						new BigDecimal(venda.getVenda().getValorDescontoStr().replace("R$", "").replace(",", ".").trim()) 
    						: new BigDecimal("0"));
    		
//    		vendaPersiste.setValorDesconto(venda.getVenda().getValorDesconto());
//    		vendaPersiste.setValorFrete(venda.getVenda().getValorFrete());
    		vendaPersiste.setTipoPagamento(venda.getTipoPagamento());
    		
    		Date dataEntrega = new DateTime(venda.getDataEntregaStr()).toDate();
    		vendaPersiste.setDataEntrega(dataEntrega);
    		
    		List<ItemVenda> itensVenda = criarItemVenda(vendaPersiste);
    		vendaPersiste.setItens(itensVenda);
    		vendaService.create(vendaPersiste);
    		
    		Compra compra = criateCompra(vendaPersiste);
    		compra.setVenda(vendaPersiste);
    		compraService.create(compra);
    		
    		return venda();
    	} else {

    		setItensVenda(new ArrayList<ItemVenda>());
    		setVenda(new VendaVO());
    		setProduto(new Produto());
    		
    		mv = new ModelAndView("/vendas/registrar");
            mv.addObject("venda", getVenda());
            mv.addObject("enderecoCompleto", "");
            mv.addObject("unidades", unidadeMedidaService.findAll());
            mv.addObject("tipoPagamentos", tipoPagamentoService.findAll());
            mv.addObject("itens", getItensVenda());
            
    		return mv;
    	}
    }

	private Compra criateCompra(Venda venda) {
		
		Compra compra = new Compra();
		compra.setDataCompra(new Date());
		List<ItemCompra> itensCompra = criarItensCompra(venda.getItens(), compra);
		compra.setItens(itensCompra);
		
		return compra;
	}

	private List<ItemCompra> criarItensCompra(List<ItemVenda> itensVenda, Compra compra) {
		
		List<ItemCompra> itens = new ArrayList<ItemCompra>();
		
		for(ItemVenda item: itensVenda) {
			
			ItemCompra itemCompra = new ItemCompra();
			itemCompra.setCategoriaProduto(item.getCategoriaProduto());
			itemCompra.setCodigo(item.getCodigo());
			itemCompra.setDescricao(item.getDescricao());
			itemCompra.setFornecedor(item.getFornecedor());
			itemCompra.setTipoProduto(item.getTipoProduto());
			itemCompra.setQuantidade(item.getQuantidade());
			itemCompra.setUnidadeMedida(item.getUnidadeMedida());
			itemCompra.setPrecoCompra(item.getPrecoCompra());
			itemCompra.setCompra(compra);
			itens.add(itemCompra);
		}
		
		return itens;
	}

	private List<ItemVenda> criarItemVenda(Venda vendaPersiste) {
		
		List<ItemVenda> itens = new ArrayList<ItemVenda>();
		
		for(ItemVenda item: itensVenda) {
			ItemVenda itemVenda = new ItemVenda();
			
			itemVenda.setCategoriaProduto(item.getCategoriaProduto());
			itemVenda.setCodigo(item.getCodigo());
			itemVenda.setDescricao(item.getDescricao());
			itemVenda.setFornecedor(item.getFornecedor());
			itemVenda.setTipoProduto(item.getTipoProduto());
			itemVenda.setQuantidade(item.getQuantidade());
			itemVenda.setPrecoVenda(item.getPrecoVenda());
			itemVenda.setUnidadeMedida(unidadeMedidaService.findById(item.getUnidadeMedida().getId()));
			
			itemVenda.setVenda(vendaPersiste);
			itens.add(itemVenda);
		}
		
		return itens;
	}
    
    @RequestMapping(value = "/vendas/addItem/", method = RequestMethod.POST)
    @ResponseBody
    public ModelAndView addItem(@RequestParam(value = "codigoProduto") String codigoProduto, 
    		@RequestParam(value = "idUnidade") Long idUnidade, 
    		@RequestParam(value = "quantidade") BigDecimal quantidade) {
    	
    	Produto produto = produtoService.findByCodigo(new Long(codigoProduto));
        UnidadeMedida unidadeMedida = unidadeMedidaService.findById(idUnidade);
        
    	setItemVenda(new ItemVenda());
    	getItemVenda().setCodigo(produto.getCodigo());
    	getItemVenda().setDescricao(produto.getDescricao());
    	getItemVenda().setCategoriaProduto(produto.getCategoriaProduto());
    	getItemVenda().setFornecedor(produto.getFornecedor());
    	getItemVenda().setTipoProduto(produto.getTipoProduto());
    	getItemVenda().setUnidadeMedida(unidadeMedida);
    	getItemVenda().setQuantidade(quantidade);
    	getItemVenda().setPrecoVenda(produto.getPrecoVenda());
    	getItemVenda().setPrecoCompra(produto.getPrecoCompra());
    	
    	getItensVenda().add(getItemVenda());
    	
    	getVenda().setListaItens(getItensVenda());
    	getVenda().setItem(getItemVenda());
    	getVenda().calculaSomaTotal();
        mv.addObject("venda", getVenda());
        mv.addObject("item", new ItemVenda());
         
        return mv;
    }
    
    @RequestMapping(value = "/vendas/addItemEdit/", method = RequestMethod.POST)
    @ResponseBody
    public ModelAndView addItemEdit(@RequestParam(value = "codigoProduto") String codigoProduto, 
    		@RequestParam(value = "idUnidade") Long idUnidade, 
    		@RequestParam(value = "quantidade") BigDecimal quantidade) {
    	
    	Produto produto = produtoService.findByCodigo(new Long(codigoProduto));
        UnidadeMedida unidadeMedida = unidadeMedidaService.findById(idUnidade);
        
    	setItemVenda(new ItemVenda());
    	getItemVenda().setCodigo(produto.getCodigo());
    	getItemVenda().setDescricao(produto.getDescricao());
    	getItemVenda().setCategoriaProduto(produto.getCategoriaProduto());
    	getItemVenda().setFornecedor(produto.getFornecedor());
    	getItemVenda().setTipoProduto(produto.getTipoProduto());
    	getItemVenda().setUnidadeMedida(unidadeMedida);
    	getItemVenda().setQuantidade(quantidade);
    	getItemVenda().setPrecoVenda(produto.getPrecoVenda());
    	getItemVenda().setPrecoCompra(produto.getPrecoCompra());
    	
    	getPedido().getItens().add(getItemVenda());
    	getPedido().calculaSomaTotal();
    	ModelAndView mv = new ModelAndView("/home/edit");
        mv.addObject("pedido", getPedido());
    	mv.addObject("item", getItemVenda());
    	mv.addObject("unidades", unidadeMedidaService.findAll());
    	mv.addObject("itens", getPedido().getItens());
    	mv.addObject("tipoPagamentos", tipoPagamentoService.findAll());
         
        return mv;
    }
    
    public ModelAndView addProduto(Produto produto) {
         
    	setItemVenda(new ItemVenda());
    	getItemVenda().setCategoriaProduto(produto.getCategoriaProduto());
    	getItemVenda().setCodigo(produto.getCodigo());
    	getItemVenda().setDescricao(produto.getDescricao());
    	getItemVenda().setFornecedor(produto.getFornecedor());
    	getItemVenda().setPrecoCompra(produto.getPrecoCompra());
    	getItemVenda().setPrecoVenda(produto.getPrecoVenda());
    	getItemVenda().setUnidadeMedida(produto.getUnidadeMedida());
    	getVenda().setItem(getItemVenda());

    	mv.addObject("venda", getVenda());
         
        return mv;
    }
    
    public ModelAndView addProdutoEdit(Produto produto) {
        
    	setItemVenda(new ItemVenda());
    	getItemVenda().setCategoriaProduto(produto.getCategoriaProduto());
    	getItemVenda().setCodigo(produto.getCodigo());
    	getItemVenda().setDescricao(produto.getDescricao());
    	getItemVenda().setFornecedor(produto.getFornecedor());
    	getItemVenda().setPrecoCompra(produto.getPrecoCompra());
    	getItemVenda().setPrecoVenda(produto.getPrecoVenda());
    	getItemVenda().setUnidadeMedida(produto.getUnidadeMedida());
    	
    	getPedido().setItem(getItemVenda());
    	
    	ModelAndView mv = new ModelAndView("/home/edit");
    	mv.addObject("pedido", getPedido());
    	mv.addObject("item", getItemVenda());
    	mv.addObject("unidades", unidadeMedidaService.findAll());
    	mv.addObject("itens", getPedido().getItens());
    	
        return mv;
    }
    
    @RequestMapping(value = "/vendas/carregarProduto/", method = RequestMethod.POST)
    @ResponseBody
    public ModelAndView carregarProduto(@RequestParam(value = "codigoProduto") String codigoProduto) {
        return addProduto(produtoService.findByCodigo(new Long(codigoProduto)));
    }
    
    @RequestMapping(value = "/vendas/carregarProdutoEdit/", method = RequestMethod.POST)
    @ResponseBody
    public ModelAndView carregarProdutoEdit(@RequestParam(value = "codigoProduto") String codigoProduto) {
        return addProdutoEdit(produtoService.findByCodigo(new Long(codigoProduto)));
    }
    
    @RequestMapping(value = "/vendas/carregarCliente/", method = RequestMethod.POST)
    @ResponseBody
    private ModelAndView carregarCliente(@RequestParam(value = "nomeCliente") String nomeCliente) {
    	List<Cliente> clientes = clienteService.findByNomeLike(nomeCliente);
    	
    	Cliente cliente = !clientes.isEmpty() ? clientes.get(0) : null;
    			
    	return addCliente(cliente);
    }
    
    public ModelAndView addCliente(Cliente cliente) {
    	
    	getVenda().setCliente(cliente);
    	mv.addObject("venda", getVenda());
    	
    	return mv;
    }
    
    @RequestMapping(value = "/vendas/delete/", method = RequestMethod.POST)
    @ResponseBody
    public ModelAndView delete(@RequestParam(value = "codigoProduto") String codigoProduto) {
        
    	List<ItemVenda> listItensRemove = new ArrayList<ItemVenda>();
    	
    	
    	
    	if(getItensVenda() != null) {
    		for (ItemVenda itemVenda : itensVenda) {
    			if(itemVenda.getCodigo().equals(new Long(codigoProduto))) {
    				listItensRemove.add(itemVenda);
    			}
			}
    		
    		for (ItemVenda itemVenda : listItensRemove) {
    			getItensVenda().remove(itemVenda);
    		}
    	}
    	
    	getVenda().calculaSomaTotal();
//    	mv = new ModelAndView("/vendas/registrar");
        mv.addObject("itens", getItensVenda());
        mv.addObject("venda", getVenda());
        mv.addObject("unidades", unidadeMedidaService.findAll());
        mv.addObject("tipoPagamentos", tipoPagamentoService.findAll());
         
        return mv;
    }
    
    @RequestMapping(value = "/vendas/deleteEdit/", method = RequestMethod.POST)
    @ResponseBody
    public ModelAndView deleteEdit(@RequestParam(value = "codigoProduto") String codigoProduto) {
        
    	List<ItemVenda> listItensRemove = new ArrayList<ItemVenda>();
    	
    	if(getPedido().getItens() != null) {
    		for (ItemVenda itemVenda : getPedido().getItens()) {
    			if(itemVenda.getCodigo().equals(new Long(codigoProduto))) {
    				listItensRemove.add(itemVenda);
    			}
			}
    		
    		for (ItemVenda itemVenda : listItensRemove) {
    			getPedido().getItens().remove(itemVenda);
    		}
    	}
    	
    	getPedido().calculaSomaTotal();
    	
    	getPedido().getItem().setUnidadeMedida(new UnidadeMedida());
    	
    	ModelAndView mv = new ModelAndView("/home/edit");
    	mv.addObject("pedido", getPedido());
    	mv.addObject("item", getPedido().getItem());
    	mv.addObject("unidades", unidadeMedidaService.findAll());
    	mv.addObject("itens", getPedido().getItens());
        mv.addObject("tipoPagamentos", tipoPagamentoService.findAll());
         
        return mv;
    }

	public Long getCodigoProduto() {
		return codigoProduto;
	}

	public void setCodigoProduto(Long codigoProduto) {
		this.codigoProduto = codigoProduto;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public List<ItemVenda> getItensVenda() {
		return itensVenda;
	}

	public void setItensVenda(List<ItemVenda> itensVenda) {
		this.itensVenda = itensVenda;
	}

	public ItemVenda getItemVenda() {
		return itemVenda;
	}

	public void setItemVenda(ItemVenda itemVenda) {
		this.itemVenda = itemVenda;
	}

	public Produto getProduto() {
		if(this.produto == null) {
			setProduto(new Produto());
		}
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public VendaVO getVenda() {
		return venda;
	}

	public void setVenda(VendaVO venda) {
		this.venda = venda;
	}

	public Venda getPedido() {
		return pedido;
	}

	public void setPedido(Venda pedido) {
		this.pedido = pedido;
	}
	
 
}