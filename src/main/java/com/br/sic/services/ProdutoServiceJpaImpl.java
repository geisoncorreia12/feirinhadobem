package com.br.sic.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.br.sic.model.Produto;
import com.br.sic.repositories.ProdutoRepository;

@Service
@Primary
public class ProdutoServiceJpaImpl implements ProdutoService {
	
	@Autowired
	private ProdutoRepository produtoRepository;
	
	@Override
	public List<Produto> findAll() {
		return produtoRepository.findAll();
	}

	@Override
	public Produto findById(Long id) {
		return produtoRepository.findOne(id);
	}

	@Override
	public Produto create(Produto produto) {
		return produtoRepository.save(produto);
	}

	@Override
	public Produto edit(Produto produto) {
		return produtoRepository.save(produto);
	}

	@Override
	public void deleteById(Long id) {
		produtoRepository.delete(id);
	}

	@Override
	public Produto findByCodigo(Long codigo) {
		return produtoRepository.findByCodigo(codigo);
	}

	@Override
	public Long ultimoCodigoSalvo() {
		final Long ultimoCodigoSalvo = produtoRepository.ultimoCodigoSalvo();
		return ultimoCodigoSalvo != null ? ultimoCodigoSalvo : 1;
	}

}
