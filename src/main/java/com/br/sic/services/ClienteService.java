package com.br.sic.services;

import java.util.List;

import com.br.sic.model.Cliente;

public interface ClienteService {
	
	public List<Cliente> findAll();

	public List<Cliente> findByNome(String nome);
	
	public List<Cliente> findByNomeLike(String nome);
	
	public Cliente findById(Long id);

	public Cliente create(Cliente Produto);
	
	public Cliente edit(Cliente Produto);
	
	public void deleteById(Long id);

}
