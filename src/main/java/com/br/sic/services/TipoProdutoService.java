package com.br.sic.services;

import java.util.List;

import com.br.sic.model.TipoProduto;

public interface TipoProdutoService {
	
	public List<TipoProduto> findAll();

}
