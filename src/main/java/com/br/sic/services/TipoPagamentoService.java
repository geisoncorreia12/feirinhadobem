package com.br.sic.services;

import java.util.List;

import com.br.sic.model.TipoPagamento;

public interface TipoPagamentoService {
	
	public List<TipoPagamento> findAll();

}
