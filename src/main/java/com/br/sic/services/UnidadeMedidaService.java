package com.br.sic.services;

import java.util.List;

import com.br.sic.model.UnidadeMedida;

public interface UnidadeMedidaService {
	
	public List<UnidadeMedida> findAll();
	
	public UnidadeMedida findById(Long id);

}
