package com.br.sic.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.br.sic.model.UnidadeMedida;
import com.br.sic.repositories.UnidadeMedidaRepository;

@Service
@Primary
public class UnidadeMedidaServiceJpaImpl implements UnidadeMedidaService {

	@Autowired
	private UnidadeMedidaRepository unidadeMedidaRepository;
	
	@Override
	public List<UnidadeMedida> findAll() {
		
		return unidadeMedidaRepository.findAll();
	}

	@Override
	public UnidadeMedida findById(Long id) {
		return unidadeMedidaRepository.findOne(id);
	}

}
