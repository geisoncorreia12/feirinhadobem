package com.br.sic.services;

import java.util.List;

import com.br.sic.model.ItemVenda;

public interface ItemVendaService {
	
	public List<ItemVenda> findAll();

	public List<ItemVenda> findPorVenda(Long idVenda);
	
	public ItemVenda findById(Long id);
	
	public ItemVenda create(ItemVenda itemVenda);
	
	public ItemVenda edit(ItemVenda itemVenda);
	
	public void deleteById(Long id);
	
}
