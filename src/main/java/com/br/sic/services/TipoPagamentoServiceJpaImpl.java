package com.br.sic.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.br.sic.model.TipoPagamento;
import com.br.sic.repositories.TipoPagamentoRepository;

@Service
@Primary
public class TipoPagamentoServiceJpaImpl implements TipoPagamentoService {
	
	@Autowired
	private TipoPagamentoRepository tipoPagamentoRepository;

	@Override
	public List<TipoPagamento> findAll() {
		return tipoPagamentoRepository.findAll();
	}

}
