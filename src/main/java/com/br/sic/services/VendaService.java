package com.br.sic.services;

import java.util.List;

import com.br.sic.model.Venda;

public interface VendaService {
	
public List<Venda> findAll();
	
	public Venda findById(Long id);
	
	public Venda create(Venda venda);
	
	public Venda edit(Venda venda);
	
	public void deleteById(Long id);
	
	List<Venda> buscarTodosPedidosAtivos();
	
	List<Venda> buscarTodosPedidosFinalizados();

}
