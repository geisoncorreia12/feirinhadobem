package com.br.sic.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.br.sic.model.Fornecedor;
import com.br.sic.repositories.FornecedorRepository;

@Service
@Primary
public class FornecedorServiceJpaImpl implements FornecedorService {
	
	@Autowired
	private FornecedorRepository fornecedorRepository;

	@Override
	public List<Fornecedor> findAll() {
		return fornecedorRepository.findAll();
	}

}
