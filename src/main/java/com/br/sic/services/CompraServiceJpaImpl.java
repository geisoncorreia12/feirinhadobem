package com.br.sic.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.br.sic.model.Compra;
import com.br.sic.repositories.CompraRepository;
import com.br.sic.vo.ItemCompraVO;

@Service
@Primary
public class CompraServiceJpaImpl implements CompraService {
	
	@Autowired
	private CompraRepository compraRepository;
	
	@Override
	public List<Compra> findAll() {
		return compraRepository.findAll();
	}

	@Override
	public Compra findById(Long id) {
		return compraRepository.findOne(id);
	}

	@Override
	public Compra create(Compra compra) {
		return compraRepository.save(compra);
	}

	@Override
	public Compra edit(Compra produto) {
		return compraRepository.save(produto);
	}

	@Override
	public void deleteById(Long id) {
		compraRepository.delete(id);
	}

	@Override
	public List<ItemCompraVO> findItensPendentes() {
		return compraRepository.findItensPendentes();
	}

	@Override
	public void deleteByVenda(Long idVenda) {
		compraRepository.deleteByVenda(idVenda);
	}

}

