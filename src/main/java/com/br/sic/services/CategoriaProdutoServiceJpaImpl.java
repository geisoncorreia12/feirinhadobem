package com.br.sic.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.br.sic.model.CategoriaProduto;
import com.br.sic.repositories.CategoriaProdutoRepository;

@Service
@Primary
public class CategoriaProdutoServiceJpaImpl implements CategoriaProdutoService {

	@Autowired
	private CategoriaProdutoRepository categoriaProdutoServiceJpaImpl;
	
	@Override
	public List<CategoriaProduto> findAll() {
		return categoriaProdutoServiceJpaImpl.findAll();
	}

}
