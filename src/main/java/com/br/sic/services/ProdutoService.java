package com.br.sic.services;

import java.util.List;

import com.br.sic.model.Produto;

public interface ProdutoService {
	
	public List<Produto> findAll();
	
	public Produto findById(Long id);

	public Produto findByCodigo(Long codigo);
	
	public Produto create(Produto Produto);
	
	public Produto edit(Produto Produto);
	
	public void deleteById(Long id);
	
	public Long ultimoCodigoSalvo();

}
