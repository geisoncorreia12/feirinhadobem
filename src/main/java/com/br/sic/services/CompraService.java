package com.br.sic.services;

import java.util.List;

import com.br.sic.model.Compra;
import com.br.sic.vo.ItemCompraVO;

public interface CompraService {
	
	public List<Compra> findAll();
	
	public Compra findById(Long id);
	
	public Compra create(Compra compra);
	
	public Compra edit(Compra compra);
	
	public void deleteById(Long id);
	
	public void deleteByVenda(Long idVenda);
	
	public List<ItemCompraVO> findItensPendentes();
	
}
