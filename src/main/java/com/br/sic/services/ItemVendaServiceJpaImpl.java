package com.br.sic.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.br.sic.model.ItemVenda;
import com.br.sic.repositories.ItemVendaRepository;

@Service
@Primary
public class ItemVendaServiceJpaImpl implements ItemVendaService {
	
	@Autowired
	private ItemVendaRepository itemVendaRepository;

	@Override
	public List<ItemVenda> findAll() {
		return itemVendaRepository.findAll();
	}

	@Override
	public ItemVenda findById(Long id) {
		return itemVendaRepository.findOne(id);
	}

	@Override
	public ItemVenda create(ItemVenda itemVenda) {
		return itemVendaRepository.save(itemVenda);
	}

	@Override
	public ItemVenda edit(ItemVenda itemVenda) {
		return itemVendaRepository.save(itemVenda);
	}

	@Override
	public void deleteById(Long id) {
		itemVendaRepository.delete(id);
	}

	@Override
	public List<ItemVenda> findPorVenda(Long idVenda) {
		return itemVendaRepository.findPorVenda(idVenda);
	}

}
