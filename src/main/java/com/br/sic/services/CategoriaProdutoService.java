package com.br.sic.services;

import java.util.List;

import com.br.sic.model.CategoriaProduto;

public interface CategoriaProdutoService {
	
	public List<CategoriaProduto> findAll();

}
