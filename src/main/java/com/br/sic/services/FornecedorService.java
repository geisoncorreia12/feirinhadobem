package com.br.sic.services;

import java.util.List;

import com.br.sic.model.Fornecedor;

public interface FornecedorService {
	
	public List<Fornecedor> findAll();

}
