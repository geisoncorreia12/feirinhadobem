package com.br.sic.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.br.sic.model.Venda;
import com.br.sic.repositories.VendaRepository;

@Service
@Primary
public class VendaServiceJpaImpl implements VendaService {
	
	@Autowired
	private VendaRepository vendaRepository;
	
	@Override
	public List<Venda> findAll() {
		return vendaRepository.findAll();
	}

	@Override
	public Venda findById(Long id) {
		return vendaRepository.findOne(id);
	}

	@Override
	public Venda create(Venda venda) {
		return vendaRepository.save(venda);
	}

	@Override
	public Venda edit(Venda venda) {
		return vendaRepository.save(venda);
	}

	@Override
	public void deleteById(Long id) {
		vendaRepository.delete(id);
	}

	@Override
	public List<Venda> buscarTodosPedidosAtivos() {
		return vendaRepository.buscarTodosPedidosAtivos();
	}

	@Override
	public List<Venda> buscarTodosPedidosFinalizados() {
		return vendaRepository.buscarTodosPedidosFinalizados();
	}

}
