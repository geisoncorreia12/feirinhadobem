package com.br.sic.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.br.sic.model.TipoProduto;
import com.br.sic.repositories.TipoProdutoRepository;

@Service
@Primary
public class TipoProdutoServiceJpaImpl implements TipoProdutoService {
	
	@Autowired
	private TipoProdutoRepository tipoProdutoServiceJpaImpl;

	@Override
	public List<TipoProduto> findAll() {
		return tipoProdutoServiceJpaImpl.findAll();
	}

}
