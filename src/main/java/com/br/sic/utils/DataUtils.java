package com.br.sic.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DataUtils {
	
	public static SimpleDateFormat format;
	
	public static String dataFormatada(String mascara, Date date) {
		
		if(date != null) {
			format = new SimpleDateFormat(mascara);
			return format.format(date);
		}
		
		return "";
	}

}
