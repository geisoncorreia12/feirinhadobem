package com.br.sic;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.br.sic.model.Cliente;
import com.br.sic.services.ClienteService;

@Controller
public class ClienteController {
	
	@Autowired
	private ClienteService clienteService;

    @GetMapping("/clientes")
    public ModelAndView findAll() {
         
        ModelAndView mv = new ModelAndView("/clientes/view");
        mv.addObject("clientes", clienteService.findAll());
        mv.addObject("cliente", new Cliente());
        mv.addObject("nome", "");
         
        return mv;
    }
    
    @GetMapping("/cliente/add")
    public ModelAndView add(Cliente cliente) {
         
        ModelAndView mv = new ModelAndView("/clientes/novo");
        mv.addObject("cliente", cliente);
        mv.addObject("enderecoEntrega", false);
         
        return mv;
    }

    @GetMapping("/cliente/consultar/{nome}")
    public ModelAndView consultar(@PathVariable("nome") String nome) {
    	
    	List<Cliente> clientes = clienteService.findByNome(nome);
    	
    	 ModelAndView mv = new ModelAndView("/clientes/view");
         mv.addObject("clientes", clientes);
         mv.addObject("cliente", new Cliente());
          
         return mv;
    	
    }
    
    @GetMapping("/cliente/edit/{id}")
    public ModelAndView edit(@PathVariable("id") Long id) {
         
        return add(clienteService.findById(id));
    }
     
    @GetMapping("/cliente/delete/{id}")
    public ModelAndView delete(@PathVariable("id") Long id) {
         
    	clienteService.deleteById(id);
         
        return findAll();
    }
 
    @PostMapping("/cliente/save")
    public ModelAndView save(@Valid Cliente cliente, BindingResult result) {
         
        if(result.hasErrors()) {
            return add(cliente);
        }
        
        if(cliente.getId() != null) {
        	clienteService.edit(cliente);
        } else {
        	clienteService.create(cliente);
        }
         
        return findAll();
    }
    
}