package com.br.sic;

import java.math.BigDecimal;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.br.sic.model.Produto;
import com.br.sic.services.CategoriaProdutoService;
import com.br.sic.services.FornecedorService;
import com.br.sic.services.ProdutoService;
import com.br.sic.services.TipoProdutoService;
import com.br.sic.services.UnidadeMedidaService;

@Controller
public class ProdutoController {
	
	@Autowired
	private ProdutoService produtoService;

	@Autowired
	private TipoProdutoService tipoProdutoService;

	@Autowired
	private CategoriaProdutoService categoriaProdutoService;

	@Autowired
	private FornecedorService fornecedorService;

	@Autowired
	private UnidadeMedidaService unidadeMedidaService;

	private ModelAndView mv; 
	
    @GetMapping("/produtos")
    public ModelAndView findAll() {
         
        mv = new ModelAndView("/produtos/view");
        mv.addObject("produtos", produtoService.findAll());
        mv.addObject("fornecedores", fornecedorService.findAll());
         
        return mv;
    }
    
    @GetMapping("/add")
    public ModelAndView add(Produto produto) {
         
        mv = new ModelAndView("/produtos/novo");
        
        produto.setCodigo(produtoService.ultimoCodigoSalvo());
        mv.addObject("produto", produto);
        mv.addObject("tipos", tipoProdutoService.findAll());
        mv.addObject("categorias", categoriaProdutoService.findAll());
        mv.addObject("fornecedores", fornecedorService.findAll());
        mv.addObject("unidades", unidadeMedidaService.findAll());
         
        return mv;
    }
    
    @GetMapping("/edit/{id}")
    public ModelAndView edit(@PathVariable("id") Long id) {
    	
    	Produto produto = produtoService.findById(id);

    	BigDecimal precoCompra = produto.getPrecoCompra();
        produto.setPrecoCompraStr(precoCompra.toString());

        BigDecimal precoVenda = produto.getPrecoVenda();
        produto.setPrecoVendaStr(precoVenda.toString());
        
    	mv = new ModelAndView("/produtos/novo");
    	mv.addObject("produto", produto);
    	mv.addObject("tipos", tipoProdutoService.findAll());
        mv.addObject("categorias", categoriaProdutoService.findAll());
        mv.addObject("fornecedores", fornecedorService.findAll());
        mv.addObject("unidades", unidadeMedidaService.findAll());
    	
    	return mv;
    }
     
    @GetMapping("/delete/{id}")
    public ModelAndView delete(@PathVariable("id") Long id) {
    	produtoService.deleteById(id);
        return findAll();
    }
    
    @PostMapping("/save")
    public ModelAndView save(@Valid Produto produto, BindingResult result) {
    	
    	String precoCompra = produto.getPrecoCompraStr().replace("R$", "").replace(",", ".").trim();
        produto.setPrecoCompra(new BigDecimal(precoCompra));

        String precoVenda = produto.getPrecoVendaStr().replace("R$", "").replace(",", ".").trim();
        produto.setPrecoVenda(new BigDecimal(precoVenda));
    	
        if(result.hasErrors()) {
            return add(produto);
        }
         
        produtoService.create(produto);
         
        return findAll();
    }
    
}