package com.br.sic.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.br.sic.model.Cliente;
import com.br.sic.model.ItemVenda;
import com.br.sic.model.TipoPagamento;
import com.br.sic.model.Venda;

public class VendaVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9101612111895135744L;

	private Long id;
	private Long idCliente;
	private Venda venda;
	private List<ItemVenda> listaItens;
	private ItemVenda item;
	private Cliente cliente;
	private TipoPagamento tipoPagamento;
	private String tipoPagamentoStr;
	private String valorDescontoStr;
	private String valorFreteStr;
	private String dataEntregaStr;
	private BigDecimal valorTotal;
	private BigDecimal valorFrete;
	private BigDecimal valorDesconto;
	private Date dataVenda;
	private Date dataEntrega;
	private String observacao;
	
	public VendaVO() {
		setVenda(new Venda());
		setListaItens(new ArrayList<ItemVenda>());
		setCliente(new Cliente());
		setItem(new ItemVenda());
	}
	
	public BigDecimal calculaSomaTotal() {
		
		BigDecimal valorTotal =  new BigDecimal(BigInteger.ZERO);
		
		if(getListaItens() != null && !getListaItens().isEmpty()) {
			for (ItemVenda itemVenda : getListaItens()) {
				valorTotal = valorTotal.add(itemVenda.calculaValorVenda());
			}
		}
		setValorTotal(valorTotal);
		
		return getValorTotal();
	}
	
	public ItemVenda getItem() {
		return item;
	}

	public void setItem(ItemVenda item) {
		this.item = item;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Venda getVenda() {
		return venda;
	}
	public void setVenda(Venda venda) {
		this.venda = venda;
	}

	public List<ItemVenda> getListaItens() {
		return listaItens;
	}
	
	public void setListaItens(List<ItemVenda> listaItens) {
		this.listaItens = listaItens;
	}

	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public TipoPagamento getTipoPagamento() {
		return tipoPagamento;
	}

	public void setTipoPagamento(TipoPagamento tipoPagamento) {
		this.tipoPagamento = tipoPagamento;
	}

	public BigDecimal getValorTotal() {
		if(valorTotal == null) {
			setValorTotal(new BigDecimal("0"));
		}
		return valorTotal;
	}

	public void setValorTotal(BigDecimal valorTotal) {
		this.valorTotal = valorTotal;
	}

	public String getDataEntregaStr() {
		return dataEntregaStr;
	}

	public void setDataEntregaStr(String dataEntregaStr) {
		this.dataEntregaStr = dataEntregaStr;
	}

	public Date getDataEntrega() {
		return dataEntrega;
	}

	public void setDataEntrega(Date dataEntrega) {
		this.dataEntrega = dataEntrega;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public BigDecimal getValorFrete() {
		return valorFrete;
	}

	public void setValorFrete(BigDecimal valorFrete) {
		this.valorFrete = valorFrete;
	}

	public Date getDataVenda() {
		return dataVenda;
	}

	public void setDataVenda(Date dataVenda) {
		this.dataVenda = dataVenda;
	}

	public BigDecimal getValorDesconto() {
		return valorDesconto;
	}

	public void setValorDesconto(BigDecimal valorDesconto) {
		this.valorDesconto = valorDesconto;
	}

	public String getTipoPagamentoStr() {
		return tipoPagamentoStr;
	}

	public void setTipoPagamentoStr(String tipoPagamentoStr) {
		this.tipoPagamentoStr = tipoPagamentoStr;
	}

	public String getValorFreteStr() {
		return valorFreteStr;
	}

	public void setValorFreteStr(String valorFreteStr) {
		this.valorFreteStr = valorFreteStr;
	}

	public String getValorDescontoStr() {
		return valorDescontoStr;
	}

	public void setValorDescontoStr(String valorDescontoStr) {
		this.valorDescontoStr = valorDescontoStr;
	}

	public Long getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}
	
}