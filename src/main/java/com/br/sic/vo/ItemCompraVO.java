package com.br.sic.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class ItemCompraVO implements Serializable {

	private static final long serialVersionUID = 732114320427814768L;
	
	private Date dataCompra;
	private Long codigo;
	private String descricao;
	private String unidadeMedida;
	private BigDecimal quantidade;
	private BigDecimal precoCompra;
	
	public ItemCompraVO() {	}
	
	public ItemCompraVO(Date dataCompra, Long codigo, String descricao, String unidadeMedida, BigDecimal quantidade, BigDecimal precoCompra) {
		super();
		this.dataCompra = dataCompra;
		this.codigo = codigo;
		this.descricao = descricao;
		this.quantidade = quantidade;
		this.unidadeMedida = unidadeMedida;
		this.precoCompra = precoCompra;
	}

	public Long getCodigo() {
		return codigo;
	}
	
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Date getDataCompra() {
		return dataCompra;
	}

	public void setDataCompra(Date dataCompra) {
		this.dataCompra = dataCompra;
	}

	public BigDecimal getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(BigDecimal quantidade) {
		this.quantidade = quantidade;
	}

	public BigDecimal getPrecoCompra() {
		return precoCompra;
	}

	public void setPrecoCompra(BigDecimal precoCompra) {
		this.precoCompra = precoCompra;
	}

	public String getUnidadeMedida() {
		return unidadeMedida;
	}

	public void setUnidadeMedida(String unidadeMedida) {
		this.unidadeMedida = unidadeMedida;
	}

}
