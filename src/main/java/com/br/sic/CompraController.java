package com.br.sic;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

//import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.br.sic.services.CompraService;
import com.br.sic.vo.ItemCompraVO;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;

@Controller(value="compraController")
public class CompraController {

	private ModelAndView mv; 
	
	@Autowired
	private CompraService compraService;
	
	@GetMapping("/compras")
    public ModelAndView venda() {

	List<ItemCompraVO> itensCompra = compraService.findItensPendentes();
		
		mv = new ModelAndView("/compras/view");
		mv.addObject("itensCompra", itensCompra); 
		
        return mv;
    }
	
	@RequestMapping(value = "/compras/relatorioVenda/", method = RequestMethod.GET)
	@ResponseBody
	public void gerarRelatorio(HttpServletResponse response)
			throws JRException, IOException {

		List<ItemCompraVO> itensCompra = compraService.findItensPendentes();

		InputStream jasperStream = this.getClass().getResourceAsStream("/reports/rel_compras.jasper");
		Map<String, Object> params = new HashMap<>();

		JasperReport jasperReport = (JasperReport) JRLoader.loadObject(jasperStream);
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params,
				new JRBeanCollectionDataSource(itensCompra));
		response.setContentType("application/x-pdf");
		response.setHeader("Content-disposition", "inline; filename=rel_compras");

		final OutputStream outStream = response.getOutputStream();
		JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);
	}
	
}
