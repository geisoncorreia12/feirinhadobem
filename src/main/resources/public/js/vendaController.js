$(document).ready(function () {
	console.log("init");
	
	
	
	//carregar cliente
	$("#nome_cliente").bind("blur", function () { 
		var nomeCliente = $("#nome_cliente").val();
		var url='/vendas/carregarCliente/';
  		console.log("nomeCliente: " +nomeCliente);
  		console.log("url: " +url);
  		console.log("content: " +$("#div_cliente").val());
  		
  		$("#body_id").load(url,{'nomeCliente':nomeCliente}, function() {
  			console.log("param: " +nomeCliente);
			//$('#nomeCliente').val('');
		});
	});

	/*$("#telefone_id").bind("blur", function () { 
		var telefone = $("#telefone_id").val();
		var url='/vendas/carregarCliente/';
		console.log("telefone: " +telefone);
		console.log("url: " +url);
		console.log("content: " +$("#div_cliente").val());
		
		$("#body_id").load(url,{'telefone':telefone}, function() {
			console.log("param: " +telefone);
			//$('#nomeCliente').val('');
		});
	});
	*/
	//carregar produto
	$("#cod_prod_id").bind("blur", function () { 
		var codProduto = $("#cod_prod_id").val();
		var url='/vendas/carregarProduto/';
		console.log("codProduto: " +codProduto);
		console.log("url: " +url);
		
		$("#body_id").load(url,{'codigoProduto':codProduto}, function() {
			console.log("param: " +codProduto);
			//$('#nomeCliente').val('');
		});
	}); 
	
	//carregar produto
	$("#cod_prod_edit_id").bind("blur", function () {
		var codProduto = $("#cod_prod_edit_id").val();
		var url='/vendas/carregarProdutoEdit/';
		console.log("codProduto edit: " +codProduto);
		console.log("url: " +url);
		
		$("#body_id").load(url,{'codigoProduto':codProduto}, function() {
			console.log("param: " +codProduto);
			//$('#nomeCliente').val('');
		});
	}); 
	
	//add produto
	$("#btn_add_item").bind("click", function () { 
		var codProduto = $("#cod_prod_id").val();
		var idUnidade = $("#unidade").val();
		var quantidade = $("#quantidade").val();
		var url='/vendas/addItem/';
		console.log("codProduto: " +codProduto);
		console.log("idUnidade: " +idUnidade);
		console.log("quantidade: " +quantidade);
		console.log("url: " +url);
		
		$("#body_id").load(url,{'codigoProduto':codProduto, 'idUnidade':idUnidade, 'quantidade':quantidade}, function() {
			console.log("param codProduto: " +codProduto);
			console.log("param idUnidade: " +idUnidade);
			console.log("param quantidade: " +quantidade);
		});
	});
	
	//add produto edit
	$("#btn_add_item_edit").bind("click", function () {
		
		var codProduto = $("#cod_prod_edit_id").val();
		var idUnidade = $("#unidade").val();
		var quantidade = $("#quantidade").val();
		var url='/vendas/addItemEdit/';
		console.log("codProduto: " +codProduto);
		console.log("idUnidade: " +idUnidade);
		console.log("quantidade: " +quantidade);
		console.log("url: " +url);
		
		$("#body_id").load(url,{'codigoProduto':codProduto, 'idUnidade':idUnidade, 'quantidade':quantidade}, function() {
			console.log("param codProduto: " +codProduto);
			console.log("param idUnidade: " +idUnidade);
			console.log("param quantidade: " +quantidade);
		});
	});
	
	//remover item
	$(".btn_excluir_item").each(function() {
		console.log($(this));
		$(this).on("click", function () {
			var linha = $(this).closest('tr').find('td:first').html();
			console.log(linha)
//			linha.remove();

			var url='/vendas/delete/';
			$("#body_id").load(url,{'codigoProduto':linha}, function() {
				console.log("param codProduto: " +linha);
			});
		});
	});
	
	$(".btn_excluir_item_edit").each(function() {
		console.log($(this));
		$(this).on("click", function () {
			var linha = $(this).closest('tr').find('td:first').html();
			console.log(linha)
//			linha.remove();

			var url='/vendas/deleteEdit/';
			$("#body_id").load(url,{'codigoProduto':linha}, function() {
				console.log("param codProduto: " +linha);
			});
		});
	});
});